import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Exercitiu2 implements ActionListener {
        JFrame frame;
        JButton button;
        JPanel panel;
        JLabel label;
        JTextField t1;
        JTextField t2;

        public Exercitiu2(){
            frame = new JFrame();
            t1 = new JTextField();
            t2 = new JTextField();
            button = new JButton("Move");
            button.addActionListener(this);
            label = new JLabel("The result is: ");
            panel = new JPanel();
            panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
            panel.setLayout(new GridLayout(0, 1));
            panel.add(t1);
            panel.add(t2);
            panel.add(button);
            panel.add(label);
            frame.add(panel, BorderLayout.CENTER);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }

        public static void main(String[] args) {
            new Exercitiu2();
        }
    @Override
    public void actionPerformed(ActionEvent e) {
            String a= t1.getText();
            String b =t2.getText();
        t1.setText("");
        t2.setText(b);
    }

    }

